local lsp = require('lsp-zero')
lsp.preset('recommended')

lsp.ensure_installed({
	'html',
	'cssls',
	'tsserver',
	'eslint',
	'sumneko_lua',
	'gopls',
	'rust_analyzer'
})

local cmp = require('cmp')
local cmp_sel = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
	['<C-p>'] = cmp.mapping.select_prev_item(cmp_sel),
	['<C-n>'] = cmp.mapping.select_next_item(cmp_sel),
	['<C-y>'] = cmp.mapping.confirm({ select = true }),
	['<C-Space>'] = cmp.mapping.complete(),
})

lsp.setup_nvim_cmp({
	mapping = cmp_mappings
})

lsp.setup()
