#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar;
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar
echo "---" | tee -a /var/log/polybar/polybar_main_mon.log 
polybar main_monitor 2>&1 | tee -a /var/log/polybar/polybar_main_mon.log & disown

echo "Bars launched..."
