local plugins = {
  -- Tree Sitter Syntax Highlighting
  { "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        -- defaults
        "vim",
        "lua",

        -- low level langs
        "c",
        "cpp",

        -- web
        "html",
        "css",
        "scss",
        "javascript",
        "typescript",
        "tsx",
        "vue",
        "json",
        "graphql",

        -- web (phoenix)
        -- "elixir",

        -- markdown
        "markdown",
        "markdown_inline",

        -- langs
        "go",
        "ruby",

        -- configs
        "yaml",
        "toml",
        "terraform",

        -- scripts
        "bash",
        "python",
      }
    },
  },
  -- LSP, Linter, Formatter
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "ansible-language-server",
        "ansible-lint",
        "gopls",
        "graphql-language-service-cli",
        "lua-language-server",
        "ruby-lsp",
        "rust-analyzer",
        "shfmt",
        "terraform-ls",
        "typescript-language-server",
        "yaml-language-server",
        "yamllint",
      }
    }
  },
  {
    "neovim/nvim-lspconfig",
       dependencies = {
     "jose-elias-alvarez/null-ls.nvim",
     config = function()
       require "custom.configs.null-ls"
     end,
   },
    config = function ()
      require "plugins.configs.lspconfig"
      require "custom.configs.lspconfig"
    end
  },
  -- Additional Plugins
  {
    "andweeb/presence.nvim",
    config = function ()
      require "custom.configs.presence"
    end,
    lazy = false
  },
  {
   "christoomey/vim-tmux-navigator",
   lazy = false,
  },
  {
    "mattn/emmet-vim",
    lazy = false,
  }
  -- {
  --   "lambdalisue/suda.nvim",
  --   config = function ()
  --    require("custom.configs.suda")
  --   end
  -- }
}

return plugins
